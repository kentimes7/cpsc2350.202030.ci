export default function (v: string[] | string, path?: string): number[] {
  if (typeof v === 'string') {
    if (v.startsWith('[')) {
      // nums=[1,2,3]
      return JSON.parse(v) as number[];
    }
    // nums=1+2
    v = v.split(/ |\+/);
  }
  if (Array.isArray(v)) {
    // nums=1&nums=2
    return v.map(v => parseInt(v, 10));
  }
  /* eslint-disable @typescript-eslint/restrict-template-expressions */
  const usage = {
    err: 'Invalid arguments',
    usage: [
      `${path}?nums=[1,2,3]`,
      `${path}?nums=1+2+3`,
      `${path}?nums=1&nums=2&nums=3`,
    ],
  };
  throw usage;
}
