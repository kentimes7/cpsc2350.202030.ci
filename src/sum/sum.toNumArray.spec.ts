import toNumArray from './sum.toNumArray';

describe('sum app', () => {
  describe('toNumArray', () => {
    ['[1,2,3]', '1+2+3', ['1', '2', '3']].forEach(v => {
      // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
      test(`${v}`, () => {
        expect(toNumArray(v)).toMatchObject([1, 2, 3]);
      });
    });

    test('undefined', () => {
      expect(() => toNumArray((undefined as unknown) as string)).toThrow();
    });
  });

  // TODO path?
});
