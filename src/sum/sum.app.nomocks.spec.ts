import express from 'express';
import request from 'supertest';
import sumApp from './sum.app';

//FULL COMPONENT TEST
describe('sum app component', () => {
  const app = express(); //note: unbound
  beforeAll(async () => await sumApp(app, '/'));

  ([
    ['/?nums=1+2+3', { sum: 6 }],
    ['/?nums=[1,2,3]', { sum: 6 }],
    ['/?nums=1&nums=2&nums=3', { sum: 6 }],
    // mixed sign
    ['/?nums=-1+2+-3', { sum: -2 }],
    ['/?nums=[1,-2,3]', { sum: 2 }],
    ['/?nums=-1&nums=-2&nums=-3', { sum: -6 }],
    // empty
    ['/?nums=[]', { sum: 0 }],
    // NaN
    ['/?nums=', { sum: null }],
    ['/?nums=a', { sum: null }],
    ['/?nums=1+a', { sum: null }],
    ['/?nums=1&nums=a', { sum: null }],
  ] as [string, { sum: number }][]).forEach(([url, expected]) => {
    test(url, async () => {
      expect.assertions(2);
      const resp = await request(app).get(url);
      expect(resp.ok).toBe(true);
      expect(resp.body).toMatchObject(expected);
    });
  });

  test('missing nums', async () => {
    expect.assertions(3);
    const resp = await request(app).get('/');
    expect(resp.ok).toBe(false);
    const errJson = JSON.parse(resp.text) as { err: string };
    expect(errJson).toHaveProperty('err');
    expect(errJson.err).toMatch(/Invalid arguments/);
  });
});
