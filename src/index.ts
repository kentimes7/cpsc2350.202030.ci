import express from 'express';
import availableRoutes from './availableRoutes';
import sumApp from './sum/sum.app';

const app = express();

Promise.all([sumApp(app, '/sum')])
  .then(() => {
    console.log(availableRoutes(app));
    const server = app.listen(process.env.PORT || 8080);
    server.on('listening', () => {
      console.log(server.address());
    });
  })
  .catch((err: unknown) => {
    console.log(err);
  });
