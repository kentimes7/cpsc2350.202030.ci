import { Application } from 'express';

/* eslint-disable */

type routeType = { route: { methods: object; path: string }};

//https://stackoverflow.com/questions/14934452/how-to-get-all-registered-routes-in-express
export default function availableRoutes (
  app: Application
): { method: string; path: string }[] {
  return (app._router.stack as routeType[])
  .filter((r: routeType) => r.route)
  .map((r: routeType) => {
    return {
      method: Object.keys(r.route.methods)[0].toUpperCase(),
      path: r.route.path,
    };
  });
};